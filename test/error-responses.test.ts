import { handler } from '../lambda/echo';
import { Errors, Event } from '../lambda/types';
import { getBadRequestResponse } from '../lambda/responses';

describe('Exceptions', (): void => {
  test('With missing query parameters', async () => {
    const event = {} as Event;

    const response = await handler(event);
    const expectedResponse = getBadRequestResponse(Errors.MISSING_QUERY_PARAMS);
    expect(response).toEqual(expectedResponse);
  });

  test('With missing message parameter', async () => {
    const event = { queryStringParameters: {} } as Event;

    const response = await handler(event);
    const expectedResponse = getBadRequestResponse(Errors.MISSING_MESSAGE_PARAM);
    expect(response).toEqual(expectedResponse);
  });
});

import { Event } from '../lambda/types';
import { handler } from '../lambda/echo';
import { getOKResponse } from '../lambda/responses';

describe('Valid Responses', (): void => {
  function getEvent(message: string): Event {
    return {
      queryStringParameters: {
        message
      }
    } as Event;
  }

  test('With message', async () => {
    const message = 'Hello world';
    const event = getEvent(message);

    const expectedResponse = getOKResponse(message);

    const response = await handler(event);
    expect(response).toEqual(expectedResponse);
  });
});

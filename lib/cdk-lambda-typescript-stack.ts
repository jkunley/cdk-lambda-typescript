import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as apigw from '@aws-cdk/aws-apigateway';

export class CdkLambdaTypescriptStack extends cdk.Stack {
  lambdaDir = 'lambda';

  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    this.buildLambdaApi('echo');
  }

  private buildLambdaApi(handlerName: string): void {
    // Lambda
    const lambdaName = handlerName + '-handler';
    const handler = new lambda.Function(this, lambdaName, {
      runtime: lambda.Runtime.NODEJS_12_X,
      // points to the transpiled JavaScript files, which are required by AWS
      code: lambda.Code.fromAsset(`${__dirname}/../${this.lambdaDir}`),
      handler: handlerName + '.handler'
    });

    // API Gateway
    const apigName = handlerName + '-endpoint';
    new apigw.LambdaRestApi(this, apigName, {
      handler,
    });
  }
}

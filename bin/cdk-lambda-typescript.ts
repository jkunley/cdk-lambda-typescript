#!/usr/bin/env node
import * as cdk from '@aws-cdk/core';
import { CdkLambdaTypescriptStack } from '../lib/cdk-lambda-typescript-stack';

const app = new cdk.App();
new CdkLambdaTypescriptStack(app, 'CdkLambdaTypescriptStack');

export enum Errors {
  MISSING_QUERY_PARAMS = 'Missing query parameters',
  MISSING_MESSAGE_PARAM = 'Missing message parameter'
}

export interface Event {
  queryStringParameters: QueryStringParams;
}

export interface QueryStringParams {
  message: string;
}

export class OKResponse {
  readonly statusCode = '200';
  body: string;
}

export class BadRequestResponse {
  readonly statusCode = '400';
  body: string;
}

import { BadRequestResponse, OKResponse } from './types';

export function getOKResponse(message: string): OKResponse {
  const response = new OKResponse();
  response.body = message;

  /* eslint-disable no-console */
  console.log({ response });

  return response;
}

export function getBadRequestResponse(error: string): BadRequestResponse {
  const response = new BadRequestResponse();
  response.body = error;

  /* eslint-disable no-console */
  console.log({ response });

  return response;
}

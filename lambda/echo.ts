import { BadRequestResponse, Errors, Event, OKResponse } from './types';
import { getBadRequestResponse, getOKResponse } from './responses';

export const handler = async (event: Event): Promise<OKResponse | BadRequestResponse> => {
  /* eslint-disable no-console */
  console.log({ event });

  if (!event.queryStringParameters) {
    return getBadRequestResponse(Errors.MISSING_QUERY_PARAMS);
  }

  const message = event.queryStringParameters.message;
  if (!message) {
    return getBadRequestResponse(Errors.MISSING_MESSAGE_PARAM);
  }

  return getOKResponse(message);
};
